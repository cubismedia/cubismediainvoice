<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $guarded = []; // or protected $fillable = ['name', 'email', 'phone', 'active'];

    public function scopeActive($invoice)
    {
        return $invoice->where('active', 1);
    }

    public function scopeInactive($invoice)
    {
        return $invoice->where('active', 0);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
