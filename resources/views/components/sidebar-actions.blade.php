<h5 class="mb-4">Actions</h5>
<ul class="list-group list-unstyled">
    <li>
        <a href="/home" class="">Active Invoices List</a>
    </li>
    <li>
        <a href="#" class="">Inactive Invoices List</a>
    </li>
    <li>
        <a href="#" class="">All Invoices List</a>
    </li>
    <li>
        <a href="#" class="">New Invoice</a>
    </li>
    <li>
        <a href="{{ route('customers.index') }}" class="">Customers List</a>
    </li>
    <li>
        <a href="{{ route('customers.create') }}" class="">Add New Customer</a>
    </li>
</ul>

