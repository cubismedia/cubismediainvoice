@extends('layouts.app')
@section('title', 'Customer ' . $customer->first_name)
@section('content')
<div class="row">
    <div class="col-2 border-r-2">@include('components.sidebar-actions')</div>
    <div class="col-10 mx-auto">
        <div class="row">
            <div class="col-12">
                <h4 class="text-gray-600 mb-6">Customer: {{ $customer->first_name }} {{ $customer->last_name }}</h4>
                <p>
                    <a class="btn btn-primary" href="{{ route('customers.edit', ['customer' => $customer]) }}">Edit</a>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-12 table-responsive">
                <table class="table table-sm table-striped">
                    <tbody>
                    <tr>
                        <td class="text-gray-600">Id:</td>
                        <td class="text-gray-700">{{ $customer->id }}</td>
                    </tr>
                    <tr>
                        <td class="text-gray-600 mb-2">First Name:</td>
                        <td class="text-gray-700 mb-2">{{ $customer->first_name }}</td>
                    </tr>
                    <tr>
                        <td class="text-gray-600 mb-2">Last Name:</td>
                        <td class="text-gray-700 mb-2">{{ $customer->last_name }}</td>
                    </tr>
                    <tr>
                        <td class="text-gray-600 mb-2">Email:</td>
                        <td class="text-gray-700 mb-2">{{ $customer->email }}</td>
                    </tr>
                    <tr>
                        <td class="text-gray-600 mb-2">Company:</td>
                        <td class="text-gray-700 mb-2">{{ $customer->company }}</td>
                    </tr>
                    <tr>
                        <td class="text-gray-600 mb-2">Address:</td>
                        <td class="text-gray-700 mb-2">{{ $customer->address }}</td>
                    </tr>
                    <tr>
                        <td class="text-gray-600 mb-2">Suite:</td>
                        <td class="text-gray-700 mb-2">{{ $customer->suite }}</td>
                    </tr>
                    <tr>
                        <td class="text-gray-600 mb-2">City:</td>
                        <td class="text-gray-700 mb-2">{{ $customer->city }}</td>
                    </tr>
                    <tr>
                        <td class="text-gray-600 mb-2">State:</td>
                        <td class="text-gray-700 mb-2">{{ $customer->state }}</td>
                    </tr>
                    <tr>
                        <td class="text-gray-600 mb-2">Zip</td>
                        <td class="text-gray-700 mb-2">{{ $customer->zip }}</td>
                    </tr>
                    <tr>
                        <td class="text-gray-600 mb-2">Country:</td>
                        <td class="text-gray-700 mb-2">{{ $customer->country }}</td>
                    </tr>
                    <tr>
                        <td class="text-gray-700 mb-2">Phone:</td>
                        <td class="text-gray-700 mb-2">{{ $customer->phone }}</td>
                    </tr>
                    <tr>
                        <td class="text-gray-600 mb-2">Fax:</td>
                        <td class="text-gray-700 mb-2">{{ $customer->fax }}</td>
                    </tr>
                    <tr>
                        <td class="text-gray-600 mb-2">Status:</td>
                        <td class="text-gray-700 mb-2">{{ $customer->active }}</td>
                    </tr>
                    <tr>
                        <td class="text-gray-600 mb-2">Created:</td>
                        <td class="text-gray-700 mb-2">{{ $customer->created_at->format('M d, Y - h:i A') }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
