@extends('layouts.app')
@section('title', 'Customers')
@section('content')
<div class="row">
    <div class="col-2 border-r-2">@include('components.sidebar-actions')</div>
    <div class="col-10 mx-auto">
        <div class="row">
            <div class="col-12">
                <h4 class="text-gray-600 mb-6">Customers List</h4>
            </div>
        </div>
        @can('create', App\Customer::class)
            <div class="row">
                <div class="col-12">
{{--                                    <p><a href="{{ route('customers.create') }}">Add New Customer</a></p>--}}
                </div>
            </div>
        @endcan
        <div class="row font-weight-bold text-xl">
            <div class="col-4">Display Name</div>
            <div class="col-4">Company</div>
            <div class="col-2">Status</div>
            <div class="col-2">Actions</div>
        </div>
        <hr>
        @foreach($customers as $customer)
            <div class="row my-2">
                <div class="col-4">
                    @can('view', $customer)
                    @endcan

                    @cannot('view', $customer)
                        {{ $customer->first_name }} {{ $customer->last_name }}
                    @endcannot
                </div>
                <div class="col-4">
                    {{ $customer->company }}
                </div>
                <div class="col-2">
                    {{ $customer->active }}
                </div>
                <div class="col-2 flex justify-between">
                    <a href="{{ route('customers.show', ['customer' => $customer]) }}">View</a>
                    <a href="{{ route('customers.edit', ['customer' => $customer]) }}">Edit</a>
                    <form action="{{ route('customers.destroy', ['customer' => $customer]) }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="text-red-600 hover:underline">Delete</button>
                    </form>
                </div>
            </div>
        @endforeach
        <div class="row pt-4">
            <div class="col-12 text-gray-700 text-xs">
                Showing {{ $customers->firstItem() }} to {{ $customers->lastItem() }} of total {{$customers->total()}} customers.
            </div>
            <div class="col-12 d-flex justify-content-center">
                {{ $customers->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
