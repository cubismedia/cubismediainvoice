@extends('layouts.app')
@section('title', 'Add New Customer')
@section('content')
    <div class="row">
        <div class="col-2 border-r-2">@include('components.sidebar-actions')</div>
        <div class="col-10 mx-auto">
            <h4 class="text-gray-600 mb-6">Add New Customer</h4>
            <div class="row">
                <div class="col-12">
                    <form action="{{ route('customers.store') }}" method="POST" class="pb-2">
                        @include('customers.form')
                        <button class="btn btn-primary" type="submit">Add Customer</button>
                        @csrf
                    </form>
                </div>
            </div>
        </div>
@endsection
