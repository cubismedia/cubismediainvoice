@extends('layouts.app')
@section('title', 'Edit Customer')
@section('content')
<div class="row">
    <div class="col-2 border-r-2">@include('components.sidebar-actions')</div>
    <div class="col-10 mx-auto">
        <h4 class="text-gray-600 mb-6">Edit Customer: {{ $customer->first_name }} {{ $customer->last_name }}</h4>

        <div class="row">
            <div class="col-12">
                <form action="{{ route('customers.update', ['customer' => $customer]) }}" method="POST" class="pb-2">
                    @method('PATCH')
                    @include('customers.form')
                    <button class="btn btn-primary" type="submit">Save</button>
                </form>
            </div>
        </div>

    </div>
</div>
@endsection
