<div class="row">
    <div class="col-3">
        <div class="form-group">
            <label class="label" for="active">Status</label>
            <select name="active" id="active" class="form-control">
                <option value="" disabled>Select customer status</option>
                @foreach($customer->activeOptions() as $activeOptionKey => $activeOptionValue)
                    <option value="{{ $activeOptionKey }}" {{ $customer->active == $activeOptionValue ? 'selected' : '' }}>{{ $activeOptionValue }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

{{--First Name--}}
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label class="label" for="first_name">First Name</label>
            <input class="form-control @error('first_name') is-danger @enderror"
                   type="text" name="first_name" id="first_name" value="{{ old('first_name') ?? $customer->first_name }}">
            @error('first_name')
            <p class="text-red-400 text-xs">{{ $errors->first('first_name') }}</p>
            @enderror
        </div>
    </div>
{{--Last Name--}}
    <div class="col-6">
        <div class="form-group">
            <label class="label" for="last_name">Last Name</label>
            <input class="form-control @error('last_name') is-danger @enderror"
                   type="text" name="last_name" id="last_name" value="{{ old('last_name') ?? $customer->last_name }}">
            @error('last_name')
            <p class="text-red-400 text-xs">{{ $errors->first('last_name') }}</p>
            @enderror
        </div>
    </div>
</div>

<div class="row">
    <div class="col-6">
        {{--Email--}}
        <div class="form-group">
            <label class="label" for="email">Email</label>
            <input class="form-control @error('email') is-danger @enderror"
                   type="text" name="email" id="email" value="{{ old('email') ?? $customer->email }}">
            @error('email')
            <p class="text-red-400 text-xs">{{ $errors->first('email') }}</p>
            @enderror
        </div>
    </div>
    <div class="col-6">
        {{--Company--}}
        <div class="form-group">
            <label class="label" for="company">Company</label>
            <input class="form-control"
                   type="text" name="company" id="company" value="{{ old('company') ?? $customer->company }}">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-4">
        {{--Address--}}
        <div class="form-group">
            <label class="label" for="address">Address</label>
            <input class="form-control"
                   type="text" name="address" id="address" value="{{ old('address') ?? $customer->address }}">
        </div>
    </div>
    <div class="col-4">
        {{--Suite--}}
        <div class="form-group">
            <label class="label" for="suite">Suite</label>
            <input class="form-control"
                   type="text" name="suite" id="suite" value="{{ old('suite') ?? $customer->suite }}">
        </div>
    </div>
    <div class="col-4">
        {{--City--}}
        <div class="form-group">
            <label class="label" for="city">City</label>
            <input class="form-control"
                   type="text" name="city" id="city" value="{{ old('city') ?? $customer->city }}">
        </div>
    </div>
</div>


<div class="row">
    <div class="col-4">
        {{--State--}}
        <div class="form-group">
            <label class="label" for="state">State</label>
            <input class="form-control"
                   type="text" name="state" id="state" value="{{ old('state') ?? $customer->state }}">
        </div>
    </div>
    <div class="col-4">
        {{--Zip--}}
        <div class="form-group">
            <label class="label" for="zip">Zip</label>
            <input class="form-control"
                   type="text" name="zip" id="zip" value="{{ old('zip') ?? $customer->zip }}">
        </div>
    </div>
    <div class="col-4">
        {{--Country--}}
        <div class="form-group">
            <label class="label" for="country">Country</label>
            <input class="form-control"
                   type="text" name="country" id="country" value="{{ old('country') ?? $customer->country }}">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-6">
        {{--Phone--}}
        <div class="form-group">
            <label class="label" for="phone">Phone</label>
            <input class="form-control"
                   type="text" name="phone" id="phone" value="{{ old('phone') ?? $customer->phone }}">
        </div>
    </div>
    <div class="col-6">
        {{--Fax--}}
        <div class="form-group">
            <label class="label" for="fax">Fax</label>
            <input class="form-control"
                   type="text" name="fax" id="fax" value="{{ old('fax') ?? $customer->fax }}">
        </div>
    </div>
</div>
@csrf
