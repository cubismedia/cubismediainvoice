-- -------------------------------------------------------------
-- TablePlus 3.6.2(322)
--
-- https://tableplus.com/
--
-- Database: invoice
-- Generation Time: 2020-08-01 20:09:05.1480
-- -------------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suite` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customers_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `invoice_payments`;
CREATE TABLE `invoice_payments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_id` bigint(20) unsigned NOT NULL,
  `payment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `invoices`;
CREATE TABLE `invoices` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` bigint(20) unsigned NOT NULL,
  `active` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `customers` (`id`, `first_name`, `last_name`, `company`, `email`, `address`, `suite`, `city`, `state`, `zip`, `country`, `phone`, `fax`, `active`, `created_at`, `updated_at`) VALUES
('8', 'Tyra', 'Hills', 'Hirthe Ltd', 'ngreen@example.com', '9000 Oberbrunner Estates Suite 265', 'Parkways', 'Aufderhartown', 'New York', '51684', 'Heard Island and McDonald Islands', '945-938-4019 x58638', '819.836.2894 x3862', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('9', 'Tyra', 'Nicolas', 'Boehm, Mann and Lakin', 'ndach@example.org', '154 Gleason Bypass', 'Lake', 'Kennedifurt', 'Missouri', '55836-5588', 'Benin', '+18783283066', '851.222.6200 x2478', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('10', 'Verda', 'D\'Amore', 'Herzog and Sons', 'devyn75@example.net', '97284 Tracey Turnpike', 'Greens', 'South Kayleetown', 'Alabama', '49023-3474', 'Swaziland', '(398) 261-3259 x6430', '(892) 225-7848', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('11', 'Raphael', 'Kovacek', 'Kris, Huels and Witting', 'amayert@example.net', '107 Leffler Drive', 'Drive', 'Faheyside', 'Oregon', '26079', 'Uganda', '345.560.0658', '1-936-673-7050', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('12', 'Claudia', 'Purdy', 'Kulas-Kuhic', 'xbednar@example.org', '8125 Heidenreich Crossing', 'Point', 'Glennaport', 'Ohio', '68302-9246', 'Bahrain', '585.771.5561 x8351', '724.927.6370 x51246', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('13', 'Audrey', 'Adams', 'Baumbach-Stamm', 'fwalsh@example.com', '489 Nitzsche Meadows', 'Square', 'Hammesmouth', 'Delaware', '46711-3548', 'Sudan', '1-607-894-9716 x6123', '941-903-5951 x671', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('14', 'Madison', 'Abernathy', 'McLaughlin, Hirthe and Bins', 'vhessel@example.org', '1895 Lind Field', 'Rapids', 'Delfinachester', 'Kansas', '81091', 'Nigeria', '825-674-9448 x261', '402.923.3424', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('15', 'Nicolette', 'Hermann', 'Kunde, Moore and Veum', 'schuster.dusty@example.org', '868 Amelie Squares', 'Place', 'New Palmaland', 'Nebraska', '81441-8344', 'Vietnam', '(459) 583-9542 x958', '(353) 764-3462 x5774', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('16', 'Aliyah', 'Bruen', 'Spinka Ltd', 'yost.benny@example.net', '17490 Garrett Key Suite 452', 'Courts', 'Lake Davidhaven', 'Connecticut', '75432', 'Bhutan', '+1-938-627-2844', '+19379003668', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('17', 'Nathan', 'Turcotte', 'Wiza, Schulist and Parisian', 'qraynor@example.com', '93779 Keyshawn Road Suite 695', 'Viaduct', 'Monahanmouth', 'Oregon', '54943', 'Latvia', '+1.721.986.9182', '+1-457-465-4999', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('18', 'Oleta', 'Skiles', 'Goodwin, Lehner and Waters', 'arlo.harvey@example.com', '18596 Schamberger Shoal', 'Viaduct', 'North Delia', 'New Jersey', '02148', 'Monaco', '(698) 226-1884 x300', '978-521-6396 x322', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('19', 'Greta', 'Stroman', 'Lebsack Inc', 'dewayne.gutkowski@example.org', '51272 Khalid Junctions Apt. 764', 'Spur', 'Lake Emerald', 'Kansas', '30229-2510', 'Mozambique', '1-423-833-6395 x654', '(705) 395-9881', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('20', 'Magdalena', 'Howell', 'Brown, Goyette and Reilly', 'jessy87@example.org', '16022 Gislason Turnpike', 'Lodge', 'South Nicoletown', 'Alaska', '18345-0479', 'Vietnam', '+1 (276) 200-7164', '487.330.8609 x548', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('21', 'Gilbert', 'Abbott', 'Macejkovic, Rempel and Schinner', 'kovacek.luella@example.net', '7182 Hauck Square Suite 542', 'Shores', 'Barrowstown', 'Colorado', '74712-1646', 'Saint Barthelemy', '(206) 990-7361 x1655', '221.975.5773', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('22', 'Leif', 'Oberbrunner', 'Schuster Group', 'mitchell.carey@example.com', '682 Twila Crossroad', 'Village', 'Adrianachester', 'Arizona', '21071', 'Kuwait', '+1-868-642-7000', '1-605-995-3934 x505', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('23', 'Herminia', 'Cronin', 'Ward-Towne', 'tabitha89@example.com', '287 Lafayette Views Apt. 879', 'Tunnel', 'New Jerald', 'Oregon', '97124-3086', 'Trinidad and Tobago', '639-963-9550', '+1 (573) 465-1313', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('24', 'Izaiah', 'Schowalter', 'Corwin PLC', 'eferry@example.org', '70639 Robel Center Suite 083', 'Lane', 'Elinorefurt', 'Kansas', '78376', 'Cameroon', '1-587-746-7833 x06377', '(836) 456-4576 x22186', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('25', 'Ibrahim', 'Metz', 'Roberts-Hodkiewicz', 'fhayes@example.com', '291 Mckenzie Mountains', 'Passage', 'Violetteland', 'New Hampshire', '73729-8625', 'Liechtenstein', '351-712-3451 x4124', '+1-770-877-4255', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('26', 'Abelardo', 'White', 'Dach LLC', 'wschulist@example.com', '6997 Elsa Hill Suite 439', 'Rest', 'O\'Connellland', 'Utah', '31661', 'Cote d\'Ivoire', '+1-689-523-6125', '1-880-853-8552', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('27', 'Wyman', 'Lind', 'Hackett Inc', 'ghills@example.com', '4560 Torphy Turnpike', 'Crossing', 'Port Joaniefort', 'Hawaii', '28654-5427', 'Egypt', '252.258.6896', '1-698-594-7462 x49044', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('28', 'Don', 'Emmerich', 'Hammes PLC', 'ywehner@example.org', '818 Cristal Highway Suite 796', 'Highway', 'Jaskolskiburgh', 'Louisiana', '79550-3251', 'Korea', '(426) 400-2671', '+1-658-306-0091', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('29', 'Aletha', 'Waters', 'Harris, Zieme and Crist', 'jillian43@example.net', '233 Douglas Curve Apt. 764', 'Valley', 'Gleichnerport', 'Arizona', '59330-2468', 'Rwanda', '552-349-2371', '267-565-1402', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('30', 'Kiara', 'Konopelski', 'Casper-Toy', 'lmills@example.com', '9637 Donnelly Via Apt. 489', 'Village', 'East Aronmouth', 'Oklahoma', '51752-1786', 'Montserrat', '(231) 422-6632 x740', '(730) 698-6165', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('31', 'Elvie', 'Oberbrunner', 'Jerde-Lemke', 'hnitzsche@example.com', '9315 Schumm Lights', 'Points', 'Armstrongfurt', 'Minnesota', '33047', 'Moldova', '(463) 228-3829', '383-636-7051', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('32', 'Monserrate', 'Stoltenberg', 'Kunde, Grimes and Schuppe', 'gerda54@example.net', '6544 Rosetta Flat Suite 107', 'Fords', 'Lolaland', 'Colorado', '97193', 'Uzbekistan', '446.683.6531 x307', '690.865.3576', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('33', 'Jerrold', 'Waelchi', 'Green-Schultz', 'dkoelpin@example.net', '30342 Hyatt Crest', 'Ranch', 'O\'Konmouth', 'Idaho', '34745-1805', 'Jersey', '484.444.7187 x5786', '736-470-4429 x8832', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('34', 'Austen', 'Schroeder', 'Lehner, Prohaska and Schinner', 'jerrell.murphy@example.com', '6738 Wisozk Lane', 'Mill', 'Lake Letitia', 'Washington', '46500-3283', 'Ecuador', '345.971.6592 x989', '1-562-623-5387 x7375', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('35', 'Sasha', 'Leannon', 'Boyer, Huel and Wolff', 'bhills@example.com', '474 Ollie Locks Apt. 026', 'Vista', 'Lake Loganberg', 'Florida', '51065-6050', 'Svalbard & Jan Mayen Islands', '1-873-267-3551', '1-598-869-4574', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('36', 'Eladio', 'Jerde', 'Hills-Bogisich', 'derek62@example.org', '699 Alessandro Brooks Apt. 128', 'Junction', 'Fadelland', 'Alaska', '78480-2970', 'Sweden', '929-676-3927 x55665', '(591) 854-3229 x363', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('37', 'Elijah', 'Aufderhar', 'VonRueden Ltd', 'brakus.santa@example.org', '364 Cummings Crossing', 'Rapids', 'Schummbury', 'Colorado', '22741-3676', 'Angola', '(739) 732-9833', '248-899-9580 x11280', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('38', 'John', 'Blick', 'Hammes PLC', 'dschiller@example.com', '5491 D\'Amore Divide Suite 130', 'Terrace', 'Lake Rhiannon', 'Utah', '47226-2484', 'Suriname', '869-325-0641 x2806', '304.648.6035', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('39', 'Jaclyn', 'Green', 'Klocko, Bernhard and Morar', 'hane.loraine@example.org', '28095 Goodwin Crest Suite 548', 'Street', 'Edythechester', 'South Carolina', '76444-5139', 'Panama', '621.971.0622', '(202) 758-6923 x687', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('40', 'Andy', 'Schultz', 'Goodwin-Wunsch', 'hilma47@example.net', '5226 Korbin Villages Suite 970', 'Village', 'Joesphbury', 'Ohio', '07158-1437', 'Djibouti', '(859) 743-9665 x33594', '263.780.5053', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('41', 'Zetta', 'McLaughlin', 'Rice-Schulist', 'fmarvin@example.com', '1576 Rosalyn Mills', 'Walk', 'Edwinhaven', 'Arkansas', '55798', 'Maldives', '1-294-635-7451 x1892', '975.218.5669', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('42', 'Alivia', 'Reichel', 'Schiller-Gislason', 'brayan87@example.net', '506 Guy Run Suite 697', 'Cove', 'New Pablo', 'Kentucky', '56880', 'Sweden', '727.270.5737 x04548', '(793) 832-4314', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('43', 'Abagail', 'Thiel', 'Parker, Williamson and Lakin', 'kunde.elton@example.net', '747 Luettgen Flats', 'Ridge', 'West Faeland', 'Iowa', '40479', 'Ireland', '637-716-5240 x0295', '964-506-7821', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('44', 'Hilario', 'Connelly', 'King-Bogisich', 'ashlynn.gutmann@example.org', '76513 May Island Apt. 577', 'Park', 'Bergnaumberg', 'Maryland', '80138', 'Botswana', '+18176454004', '1-539-840-9698 x9084', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('45', 'Urban', 'Borer', 'Hamill-Rosenbaum', 'kris.moshe@example.com', '57734 Lionel Mountains Suite 679', 'Radial', 'New Saigeborough', 'Kentucky', '42578', 'Bahrain', '(398) 281-0433', '+1.329.965.5715', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('46', 'Dalton', 'Schneider', 'Schaden, Pagac and Lemke', 'torphy.eula@example.org', '9513 Elenora Lock Apt. 240', 'Hills', 'Abbottmouth', 'Indiana', '90649-2273', 'Netherlands Antilles', '+1-979-835-8131', '+1-330-318-9848', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('47', 'Torrance', 'Howell', 'Koss Ltd', 'ankunding.christop@example.net', '664 Dimitri Loaf', 'Stream', 'Pinkieborough', 'Montana', '79275', 'New Zealand', '691.450.4343 x220', '639-984-8807', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('48', 'Thomas', 'Schulist', 'Wolff, Stoltenberg and Gutmann', 'ike.kihn@example.org', '290 Mariana Stravenue Apt. 570', 'Squares', 'Timothymouth', 'Rhode Island', '07643', 'Sao Tome and Principe', '876-522-5594 x2607', '1-332-979-9728', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('49', 'Coty', 'Bernhard', 'Trantow-Ryan', 'hahn.bria@example.net', '823 Hickle Knolls Apt. 324', 'Skyway', 'Kilbackshire', 'Utah', '12086', 'Bulgaria', '1-230-784-2307', '559.991.1183 x194', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('50', 'Tremaine', 'Blick', 'Breitenberg-Becker', 'maribel71@example.com', '2081 Dejon Avenue Apt. 504', 'Forges', 'Noemyborough', 'Massachusetts', '99450-0027', 'Namibia', '1-982-486-9409 x1516', '915-636-3571 x2312', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('51', 'Dustin', 'Heathcote', 'Cassin, Reilly and Leffler', 'jast.maud@example.com', '442 Hansen Fork Suite 142', 'Mountains', 'Schillerberg', 'Idaho', '40648-3537', 'Bolivia', '384.345.4150 x68903', '227-400-6294 x530', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('52', 'Alessandra', 'Keebler', 'Smitham, Price and Becker', 'roosevelt83@example.net', '524 Koch Freeway Apt. 284', 'Drives', 'South Ayden', 'Ohio', '06564-6197', 'Zimbabwe', '(339) 313-9749 x3804', '1-932-588-5724 x589', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('53', 'Pietro', 'Stark', 'Purdy and Sons', 'beau34@example.com', '6601 Walter Greens', 'Springs', 'East Barryshire', 'Rhode Island', '99712-2348', 'Samoa', '+1-254-739-6716', '(687) 410-8364', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('54', 'Brenna', 'McDermott', 'Flatley-Heidenreich', 'jonathon.schmidt@example.org', '2581 Baumbach Courts Suite 282', 'Dale', 'Parisianstad', 'North Dakota', '02301', 'French Polynesia', '397.630.9541 x556', '(916) 674-3235 x436', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('55', 'Bruce', 'Kuphal', 'Cremin Inc', 'onolan@example.org', '407 Kovacek Avenue', 'Alley', 'Litzyburgh', 'New Hampshire', '75271', 'Lesotho', '576-439-9799', '462.441.7549 x390', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('56', 'Tressie', 'Kshlerin', 'Fisher Group', 'weber.angel@example.net', '4869 Bins Unions', 'Harbors', 'East Marianatown', 'Washington', '31304', 'Honduras', '1-359-623-0741', '874.521.6881', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('57', 'Josh', 'Wintheiser', 'Hickle LLC', 'brandt.von@example.com', '252 Concepcion View Suite 897', 'Hill', 'Audreanneborough', 'Pennsylvania', '05850', 'Seychelles', '795-613-3502', '356-926-8794', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('58', 'Keshaun', 'Little', 'Yundt Group', 'hackett.alysson@example.net', '5954 Bartell Mountain Suite 198', 'Gardens', 'North Joshuaborough', 'Kentucky', '94692', 'Bulgaria', '+1-902-716-9655', '947-801-3549', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('59', 'Audra', 'Schroeder', 'White Group', 'tevin.funk@example.org', '9505 Ward Estate', 'Forges', 'New Kyra', 'Minnesota', '96518-3640', 'Colombia', '501-710-3413', '(776) 379-9061', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('60', 'Bret', 'Deckow', 'Weimann LLC', 'botsford.reuben@example.com', '19798 Chanelle Lodge', 'Run', 'Camylleburgh', 'South Dakota', '55912-8327', 'Ukraine', '+14803349596', '402-799-3054', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('61', 'Amanda', 'King', 'Kemmer-Waters', 'della.gibson@example.com', '30979 VonRueden Ferry', 'Center', 'West Viva', 'Hawaii', '27730-9582', 'Burundi', '978-265-4442 x89243', '1-950-745-8092 x879', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('62', 'Arianna', 'Gottlieb', 'Crist LLC', 'bella55@example.com', '5608 Aliya Wall', 'Tunnel', 'West Immanuelport', 'Alaska', '51658', 'Finland', '1-602-672-3383', '1-906-271-9225 x0312', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('63', 'Jenifer', 'Hamill', 'Gerhold, Wintheiser and Wolf', 'kathleen.haag@example.com', '556 Jacobi Centers', 'Road', 'Osvaldoshire', 'New Mexico', '74095-5105', 'Andorra', '+1-904-329-0104', '(778) 570-1220 x9939', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('64', 'Gaston', 'McDermott', 'Gutkowski and Sons', 'thompson.lester@example.org', '1797 Osinski Overpass', 'Cliffs', 'Hilmahaven', 'Missouri', '75832', 'Niue', '1-703-684-0871 x01047', '+1-561-881-3770', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('65', 'Leatha', 'Lubowitz', 'Haag Group', 'wschmitt@example.com', '97524 Conroy Springs Suite 567', 'Coves', 'West Aronside', 'Oregon', '76185', 'Palau', '1-570-640-7543', '898.313.0778', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('66', 'Buford', 'Russel', 'Steuber, Schmidt and Satterfield', 'javon88@example.org', '827 Kevin Fields', 'Brooks', 'Port Kimberlymouth', 'Utah', '22350-8729', 'Tanzania', '1-347-712-6456 x775', '802-791-6675 x12599', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('67', 'Enid', 'Pagac', 'Dickinson Group', 'thad36@example.com', '382 Bradtke Stravenue', 'Gardens', 'O\'Konhaven', 'Florida', '71167-8846', 'Nauru', '1-881-763-7024 x5900', '578.659.9407 x13584', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('68', 'Kay', 'Mraz', 'Zieme and Sons', 'gosinski@example.com', '368 Trantow Plains', 'Burgs', 'Lysanneshire', 'New Jersey', '69459', 'Slovenia', '(361) 353-1851', '369-744-3690', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('69', 'Marlee', 'Deckow', 'Schultz-Parker', 'jrutherford@example.com', '500 Adelle Land', 'Extensions', 'West Amya', 'New Hampshire', '35141-7703', 'Nauru', '873-313-0657', '372-563-6781 x03736', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('70', 'Beau', 'Grady', 'Thiel-Reichel', 'jlabadie@example.net', '559 Hamill Neck Apt. 755', 'Port', 'Olaville', 'Massachusetts', '09977-0078', 'Ethiopia', '407.810.3760', '+1-243-305-5174', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('71', 'Amari', 'Walker', 'Feil and Sons', 'alene.barton@example.org', '652 Maryjane Shore Suite 065', 'Turnpike', 'Sauerhaven', 'Colorado', '90211', 'United States Minor Outlying Islands', '1-710-432-1475', '+13948168057', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('72', 'Annamae', 'Koepp', 'Daugherty, Lockman and Koss', 'erdman.kacie@example.com', '77024 Kling Tunnel Suite 465', 'Trail', 'New Audiebury', 'District of Columbia', '74476', 'Suriname', '1-629-568-2287 x184', '+1-373-537-0220', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('73', 'Hortense', 'Waters', 'Miller, White and Littel', 'solon08@example.org', '297 Quigley Crossroad', 'Prairie', 'West Alberta', 'Kansas', '46978-6680', 'Norway', '(739) 380-5039', '(373) 264-7685 x54808', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('74', 'Anya', 'Herzog', 'Volkman, Beier and Hudson', 'jadyn41@example.org', '732 Cremin Turnpike Apt. 764', 'Mall', 'O\'Connerfort', 'Michigan', '07949', 'Barbados', '(264) 489-6011 x7206', '1-517-266-4684 x08548', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('75', 'Miguel', 'Trantow', 'Russel-Bernier', 'parker.agustin@example.com', '969 Heller Flat Suite 445', 'Knoll', 'Ornland', 'Florida', '38720', 'Guatemala', '+1-372-687-1732', '(329) 269-7797', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('76', 'Katarina', 'Kerluke', 'Lehner and Sons', 'kris.pascale@example.com', '512 Priscilla Centers', 'Isle', 'North Rodrickfort', 'New Jersey', '20572-3060', 'Argentina', '980.952.4175', '1-516-949-1859 x2541', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('77', 'Noelia', 'Rice', 'Funk-O\'Reilly', 'little.kenton@example.com', '2842 Vern Haven Suite 345', 'Heights', 'Vincenzoton', 'Iowa', '81379', 'Germany', '227-837-8184 x1303', '1-527-414-1220 x3546', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('78', 'Davion', 'Herman', 'Weimann Ltd', 'hammes.lucio@example.net', '5804 Lysanne Forks', 'Harbor', 'South Vita', 'Utah', '68260', 'Portugal', '649.381.5980', '+1.453.882.9013', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('79', 'Cyrus', 'Stanton', 'Weissnat-Rippin', 'don07@example.org', '61565 Mustafa Vista', 'Points', 'Starkmouth', 'Georgia', '50347', 'French Polynesia', '938-488-4343', '1-494-468-6353 x77376', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('80', 'Kelton', 'Cole', 'Armstrong, Blanda and Turner', 'kcollins@example.org', '110 Makayla Roads Apt. 535', 'Fords', 'West Herminia', 'Maine', '42151-4462', 'Jamaica', '(443) 299-5703', '(780) 712-1055', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('81', 'Bettye', 'Sanford', 'Legros PLC', 'curt.jenkins@example.net', '7557 Lehner Islands Suite 241', 'Wells', 'Lake Grayceton', 'District of Columbia', '30012-8420', 'Bangladesh', '+1.380.512.7293', '(626) 646-6958', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('82', 'Angel', 'West', 'McKenzie, Jones and Prohaska', 'maye57@example.net', '84610 Kulas Shoals', 'Station', 'New Sonnytown', 'Iowa', '63248-9847', 'Hong Kong', '1-624-649-1887', '(775) 220-0464', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('83', 'Isaias', 'Windler', 'Daugherty-Lindgren', 'vincenzo19@example.net', '3867 Smith Trafficway Suite 781', 'Court', 'East Julian', 'New Mexico', '23120-6107', 'Bermuda', '648.532.9826 x090', '578-397-2769 x854', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('84', 'Nathaniel', 'Franecki', 'Brakus-Williamson', 'cruz.kohler@example.net', '512 Jacobs Ways', 'Estate', 'Lake Karenshire', 'South Carolina', '67965-3319', 'Paraguay', '1-218-284-8934 x76159', '(871) 430-4221 x670', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('85', 'Sarah', 'Stracke', 'Weimann, Huel and Hegmann', 'kuphal.maryjane@example.com', '23913 Pacocha Mountain', 'Corner', 'East Mikeburgh', 'North Dakota', '75564', 'Barbados', '(679) 338-0808 x068', '+1-895-372-5926', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('86', 'Preston', 'Thiel', 'Becker, McDermott and Hill', 'ykoch@example.net', '4609 Ryder Green Suite 711', 'Harbor', 'Runolfssonside', 'Connecticut', '68301', 'Papua New Guinea', '1-945-822-8858', '(738) 209-3407 x74372', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('87', 'Montana', 'Bednar', 'Eichmann-Kuhlman', 'ken99@example.net', '14192 Colleen Stream', 'Islands', 'Danielburgh', 'North Dakota', '26810', 'Sao Tome and Principe', '(601) 801-6049', '407.739.9173', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('88', 'Jeffry', 'Nolan', 'Rolfson PLC', 'gulgowski.alfonso@example.com', '809 Stacey Heights', 'Camp', 'East Nathanstad', 'Nebraska', '59627-3083', 'Cote d\'Ivoire', '256.230.8476', '(909) 569-4168', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('89', 'Salvador', 'Kiehn', 'Pacocha-Kuphal', 'lortiz@example.org', '908 Runte Manors', 'Mountain', 'New Patricia', 'Iowa', '99384', 'Slovenia', '1-401-762-4859 x188', '641-919-7461', '1', '2020-08-01 16:26:54', '2020-08-01 16:26:54'),
('90', 'Amani', 'Haag', 'Bauch LLC', 'gfeeney@example.net', '4604 Rylee Route Apt. 270', 'Vista', 'Hansenville', 'Utah', '33607', 'Sri Lanka', '605.409.7331', '(756) 331-6949 x55961', '0', '2020-08-01 16:26:54', '2020-08-01 16:26:54');

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
('1', '2014_10_12_000000_create_users_table', '1'),
('2', '2014_10_12_100000_create_password_resets_table', '1'),
('3', '2019_08_19_000000_create_failed_jobs_table', '1'),
('4', '2020_07_17_194720_create_customers_table', '1'),
('5', '2020_07_31_180858_create_invoices_table', '1'),
('6', '2020_07_31_182643_create_invoice_payments_table', '1');

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
('1', 'Teo Dereta', 'tdereta@gmail.com', NULL, '$2y$10$Iy3uqdLcep930Ok2HUByKenZGqluMcVLn3eGMkLpwP69ehmW6W3Ky', NULL, '2020-08-01 16:34:12', '2020-08-01 16:34:12');



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;